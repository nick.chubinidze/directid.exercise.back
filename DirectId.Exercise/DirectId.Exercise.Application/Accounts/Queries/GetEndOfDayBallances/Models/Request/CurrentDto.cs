﻿namespace DirectId.Exercise.Application.Accounts.Queries.GetEndOfDayBallances.Models
{
    public class CurrentDto
    {
        public decimal Amount { get; set; }

        public string CreditDebitIndicator { get; set; }
    }
}
