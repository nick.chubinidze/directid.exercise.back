﻿using System.Collections.Generic;

namespace DirectId.Exercise.Application.Accounts.Queries.GetEndOfDayBallances.Models
{
    public class AccountDto
    {
        public string AccountId { get; set; }

        public string CurrencyCode { get; set; }

        public string DisplayName { get; set; }

        public string AccountType { get; set; }

        public string AccountSubType { get; set; }

        public IdentifierDto Identifiers { get; set; }

        public BalanceDto Balances { get; set; }

        public List<TransactionDto> Transactions { get; set; } = new List<TransactionDto>();
    }
}
