﻿namespace DirectId.Exercise.Application.Accounts.Queries.GetEndOfDayBallances.Models
{
    public class BalanceDto
    {
        public CurrentDto Current { get; set; }

        public AvailableDto Available { get; set; }
    }
}
