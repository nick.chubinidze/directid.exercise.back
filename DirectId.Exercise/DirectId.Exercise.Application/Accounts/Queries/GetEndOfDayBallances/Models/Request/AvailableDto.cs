﻿namespace DirectId.Exercise.Application.Accounts.Queries.GetEndOfDayBallances.Models
{
    public class AvailableDto
    {
        public decimal Amount { get; set; }

        public string CreditDebitIndicator { get; set; }
    }
}
