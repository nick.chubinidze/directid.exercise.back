﻿namespace DirectId.Exercise.Application.Accounts.Queries.GetEndOfDayBallances.Models
{
    public class IdentifierDto
    {
        public string SortCode { get; set; }

        public string AccountNumber { get; set; }

        public string SecondaryIdentification { get; set; }
    }
}
