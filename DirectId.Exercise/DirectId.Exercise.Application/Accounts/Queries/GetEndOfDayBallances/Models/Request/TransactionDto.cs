﻿using System;

namespace DirectId.Exercise.Application.Accounts.Queries.GetEndOfDayBallances.Models
{
    public class TransactionDto
    {
        public string Description { get; set; }

        public decimal Amount { get; set; }

        public string CreditDebitIndicator { get; set; }

        public string Status { get; set; }

        public DateTime bookingDate { get; set; }
    }
}
