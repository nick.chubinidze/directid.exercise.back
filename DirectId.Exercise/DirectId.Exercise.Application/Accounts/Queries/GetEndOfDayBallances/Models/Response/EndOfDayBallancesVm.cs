﻿using System.Collections.Generic;

namespace DirectId.Exercise.Application.Accounts.Queries.GetEndOfDayBallances.Models
{
    public class EndOfDayBallancesVm
    {
        public decimal TotalCredits { get; set; }

        public decimal TotalDebits { get; set; }

        public List<DayBalancesDto> EndOfDayBalances { get; set; } = new List<DayBalancesDto>();
    }
}
