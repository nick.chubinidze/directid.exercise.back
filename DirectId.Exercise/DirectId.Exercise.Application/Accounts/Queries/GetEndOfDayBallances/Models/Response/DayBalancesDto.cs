﻿using System;

namespace DirectId.Exercise.Application.Accounts.Queries.GetEndOfDayBallances.Models
{
    public class DayBalancesDto
    {
        public DateTime Date { get; set; }

        public decimal Balance { get; set; }
    }
}
