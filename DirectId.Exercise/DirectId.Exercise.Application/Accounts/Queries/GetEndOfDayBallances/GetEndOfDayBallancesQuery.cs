﻿using DirectId.Exercise.Application.Accounts.Queries.GetEndOfDayBallances.Models;
using DirectId.Exercise.Application.Constants;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DirectId.Exercise.Application.Accounts.Queries.GetEndOfDayBallances
{
    public class GetEndOfDayBallancesQuery : IRequest<IEnumerable<EndOfDayBallancesVm>>
    {
        public string BrandName { get; set; }
        public string DataSourceName { get; set; }
        public string DataSourceType { get; set; }
        public DateTime RequestDateTime { get; set; }
        public IList<AccountDto> Accounts { get; set; }
    }

    public class GetEndOfDayBallancesQueryHandler : IRequestHandler<GetEndOfDayBallancesQuery, IEnumerable<EndOfDayBallancesVm>>
    {
        public async Task<IEnumerable<EndOfDayBallancesVm>> Handle(GetEndOfDayBallancesQuery request, CancellationToken cancellationToken)
        {
            var result = new List<EndOfDayBallancesVm>();

            foreach (var account in request.Accounts)
            {
                var endOfDayBalances = new EndOfDayBallancesVm
                {
                    TotalCredits = account.Transactions.Where(m => m.CreditDebitIndicator == CreditDebitIndicatorConstants.Credit).Sum(m => Math.Abs(m.Amount)),
                    TotalDebits = account.Transactions.Where(m => m.CreditDebitIndicator == CreditDebitIndicatorConstants.Debit).Sum(m => Math.Abs(m.Amount))
                };

                var dayTransactions = 
                    account.Transactions
                    .GroupBy(x => x.bookingDate.Date)
                    .OrderByDescending(x => x.Key);

                var currentBalance = account.Balances.Current.CreditDebitIndicator == CreditDebitIndicatorConstants.Credit ?
                      -Math.Abs(account.Balances.Current.Amount)
                    : Math.Abs(account.Balances.Current.Amount);

                foreach (var dayTransaction in dayTransactions)
                {
                    var credit = dayTransaction.Where(m => m.CreditDebitIndicator == CreditDebitIndicatorConstants.Credit).Sum(m => Math.Abs(m.Amount));
                    var debit = dayTransaction.Where(m => m.CreditDebitIndicator == CreditDebitIndicatorConstants.Debit).Sum(m => Math.Abs(m.Amount));

                    var balance = debit - credit;
                    currentBalance += balance;

                    endOfDayBalances.EndOfDayBalances.Add(new DayBalancesDto
                    {
                        Date = dayTransaction.Key,
                        Balance = currentBalance
                    });
                }

                result.Add(endOfDayBalances);
            }

            return await Task.FromResult(result);
        }
    }
}
