﻿namespace DirectId.Exercise.Application.Constants
{
    static class CreditDebitIndicatorConstants
    {
        public const string Credit = "Credit";
        public const string Debit = "Debit";
    }
}
