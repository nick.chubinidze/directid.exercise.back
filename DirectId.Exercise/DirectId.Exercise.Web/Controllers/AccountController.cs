﻿using DirectId.Exercise.Application.Accounts.Queries.GetEndOfDayBallances;
using DirectId.Exercise.Application.Accounts.Queries.GetEndOfDayBallances.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DirectId.Exercise.Web.Controllers
{
    public class AccountController : ApiControllerBase
    {
        [HttpPost]
        [Route("GetEndOfDayBalances")]
        public async Task<ActionResult<IEnumerable<EndOfDayBallancesVm>>> GetEndOfDayBalancesAsync(GetEndOfDayBallancesQuery getEndOfDayBallancesQuery)
        {
            return await Mediator.Send(getEndOfDayBallancesQuery) as List<EndOfDayBallancesVm>;
        }
    }
}
